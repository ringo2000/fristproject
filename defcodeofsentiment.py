from textblob import TextBlob
import pandas as pd
import time


def countSentences(textWantToCount):
    text = textWantToCount
    blob = TextBlob(text)  # print(blob.tags)
    for sentence in blob.sentences:
        countResult = str(sentence.sentiment.polarity)
    return countResult
# return [str(sentence.sentiment.polarity) for sentence in (TextBlob(textWantToCount)).sentences]
# ValueError: 1 columns passed, passed data had 6 columns


def getText():
    return [countSentences(str(x)) for x in pd.read_csv(r"csvResultOfSentiment.csv")["eng"].head()]


def toDataFrame():
    return pd.DataFrame(getText(), columns=['rank'])


def toCsv():
    return toDataFrame().to_csv(r'sentimenttest.csv')


def main():
    start = time.perf_counter()
    toCsv()
    end = time.perf_counter()
    print(f"Finish, execution time of the program is:{end - start}")


if __name__ == '__main__':
    main()
