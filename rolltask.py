import random


class RollGame:  # define class

    def dice(self):  # roll
        roll = random.randrange(1, 6)
        self.rollResult = roll

    def diceTwoTimes(self):  # roll twice
        roll1 = RollGame()
        roll1.dice()
        roll2 = RollGame()
        roll2.dice()
        self.rollTwoTimesResult = roll1.rollResult+roll2.rollResult

    def checkIfWin(self):
        ifWin = RollGame()
        ifWin.diceTwoTimes()
        rollResult = ifWin.rollTwoTimesResult  # determine win or lose
        print("Your frist roll is: " + str(rollResult))
        if rollResult in (7, 11):
            print("You're winner")

        elif rollResult in (4, 6, 8, 9, 10):
            print("Try again")
            diceAgain = RollGame()
            diceAgain.dice()
            print("Your second roll is: " + str(diceAgain.rollResult))
            if diceAgain.rollResult == rollResult:
                print("You're winner")
            else:
                print("You lose")
        else:
            print("You lose")


result = RollGame()
result.checkIfWin()
