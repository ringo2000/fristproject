from textblob import TextBlob
import pandas as pd


class Sentiment():

    def countSentences(self, textWantToCount):  # Count the rank of sentence

        text = textWantToCount
        blob = TextBlob(text)
        for sentence in blob.sentences:
            countResult = str(sentence.sentiment.polarity)
        self.getSentimentResult = countResult

    def getText(self):  # Get the text from the file

        listResult = []
        result1 = Sentiment()
        for x in pd.read_csv(r"csvResultOfSentiment.csv")["eng"]:
            result1.countSentences(str(x))
            listResult.append(result1.getSentimentResult)
        self.getListResult = listResult

    def toDataFrame(self):  # Transform to DataFrame 

        sentiment = Sentiment()
        sentiment.getText()
        dfListResult = pd.DataFrame(sentiment.getListResult, columns=['rank'])
        self.getDfListResult = dfListResult

    def main(self):  # Export to csv
        toCsv = Sentiment()
        toCsv.toDataFrame()
        toCsv.getDfListResult.to_csv(r'sentiment.csv')
        print("finish")


if __name__ == '__main__':
    callClass = Sentiment()
    callClass.main()
