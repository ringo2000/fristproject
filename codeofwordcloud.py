import pandas as pd
from wordcloud import WordCloud
import matplotlib.pyplot as plt

df = pd.read_csv(r"csvResultOfSentiment.csv")

df = df[df['eng'] != 'NoneComment']  # don't show NoneComment
# print(df.shape)
df = df[df['rate'] < 0]   # only positive comment

text1 = " ".join(str(review) for review in df.eng)  # transform to string

wordcloud = WordCloud(max_font_size=50,
                      max_words=50,
                      background_color="white").generate(text1)
plt.figure()
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()
